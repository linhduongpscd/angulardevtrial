import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './components/product/product.component';
import { TopBannerComponent } from './components/product/top-banner/top-banner.component';
import { ProductItemComponent } from './components/product/product-item/product-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    TopBannerComponent,
    ProductItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
