import { Component, OnInit } from '@angular/core';
import * as fakeData from '../../../data/fake_data.json';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  products = fakeData;

  constructor() { }

  ngOnInit() {
  }

}
